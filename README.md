BoV Default Server Role
=========

Default role for all BoV servers, manages packages, shell, users and access.

Requirements
------------

* debian based distribution

Example Playbook
----------------

```yaml
---
- name: "Base configuration"
  hosts:
    - virtual-servers
  roles:
    - role: base.role
      vars:
        locales:
          - en_US.UTF-8
        base_packages:
          - vim
          - curl
        prompt_env: "ORG-DEV"
        ca_certificates:
          - internal_ca.crt
          - internal_ca_intermediate_2020.crt
        postfix_relay: "smtp.internal.example.org"
        postfix_networks: "10.0.0.0/24"
  vars_files:
    - /etc/ansible/vars/base.yml
```

License
-------

MIT

Author Information
------------------

Jindrich Skupa
